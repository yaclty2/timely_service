<?php
/**
 * Created by PhpStorm.
 * Author: baihu
 * Date: 2019/12/16
 * Time: 13:16
 */

namespace app\admin\controller;

use Logic\AdminLogic;
class Login extends Base
{
    protected $_require_login = false;
    /**
     * 登录展示
     * @param $param
     * @return array
     */
    public  function  index(){
        return $this->fetch();
    }
    /**
     *  登录
     * @param $param
     * @return array
     */
    public  function  loginIn(){
        $param = input('param.');
        if(!captcha_check($param['captcha'])){
            return $this->response->api('', self::ERROR_CODE, '验证码错误');
        }
        $info = AdminLogic::LoginAdmin($param['username'], $param['password']);
        // 设置session标识状态
        session('admin_name', $info['account']);
        session('admin_id', $info['id']);
        return $this->response->api(url('index/index'), self::SUCCESS_CODE, '登录成功');

    }
    /**
     *  退出登录
     * @param $param
     * @return array
     */
    public function loginOut()
    {
        session('admin_name', null);
        session('admin_id', null);
        return $this->response->api(url('login/index'), self::SUCCESS_CODE, '退出登录成功');
    }



}
